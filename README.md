# TITRE

TITRE - Maxime Gonthier, Loris Marchal and Samuel Thibault

All the informations are in this depot:

```bash
~$ git clone --single-branch --branch BRANCHE https://gitlab.inria.fr/starpu/locality-aware-scheduling.git
```

You can find in this depot the code used for our experiments, our simulations and the code used in Starpu.
It also contains the data files as well as the Figures showing curves used in the paper.
You can re-create the figures used in the paper by doing the following steps.

## Installation

### To install automatically all the packages needed you can do:

```bash
~/locality-aware-scheduling$ bash Script/Install_packages.sh
```

### In order to genereate the figures you need these packages in R:

```bash
~/locality-aware-scheduling$ R
> install.packages("ggplot2")
> install.packages("tidyverse")
> q()
> n
```

### To install Simgrid for the simulations:

```bash
~/locality-aware-scheduling$ wget https://framagit.org/simgrid/simgrid/uploads/98ec9471211bba09aa87d7866c9acead/simgrid-3.26.tar.gz
~/locality-aware-scheduling$ tar xf simgrid-3.26.tar.gz
~/locality-aware-scheduling$ bash Script/Install_simgrid.sh
```

### To install Starpu

```bash
~/locality-aware-scheduling$ git clone https://gitlab.inria.fr/starpu/starpu.git 
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling/starpu$ git checkout COMMIT
~/locality-aware-scheduling/starpu$ ./autogen.sh
~/locality-aware-scheduling/starpu$ ./configure --enable-simgrid --disable-mpi
~/locality-aware-scheduling/starpu$ make
~/locality-aware-scheduling/starpu$ sudo make install
~/locality-aware-scheduling/starpu$ sudo ldconfig
~/locality-aware-scheduling/starpu$ export STARPU_PERF_MODEL_DIR=tools/perfmodels/sampling
```

## Running experiments on Grid5000

All our figures except two are done in real on Grid5000. A MODIFIER SI BESOIN.

You first need an account on Grid5000: https://www.grid5000.fr/w/Grid5000:Get_an_account

You will then be granted a login corresponding to your name that we note *yourname* in the following commands.

You can then connect to the appropriate machine:
```bash
~/$ ssh yourname@access.grid5000.fr 
~/$ ssh lyon
~/$ git clone https://gitlab.inria.fr/starpu/starpu.git 
~/$ cd starpu/
~/$ git checkout COMMIT
~/$ oarsub -t exotic -p "network_address in ('gemini-1.lyon.grid5000.fr')" -r "XXXX-XX-XX XX:XX:XX" -l walltime=04:00:00
```
With the current date and hour instead of "XXXX-XX-XX XX:XX:XX".

You will be granted a machine associated with an *ID*. With this *ID* you can connect to it with:
```bash
~/$ oarsub -C ID
```

Then, to start the experiments do:
```bash
~/$ cd starpu/
~/starpu$ bash Scripts_maxime/DEPOT/Experiments.sh yourname
```

The experiments takes about 8 hours to complete. A quicker script, but computing less points is available:
```bash
~/$ cd starpu/
~/starpu$ bash Scripts_maxime/DEPOT/Quick_Experiments.sh yourname
```

Once the experiments are finished you can logout of Grid5000 with:
```bash
~/$ exit
```
And run the following command to draw the figures:
```bash
~/$ cd locality-aware-scheduling/starpu/
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/DEPOT/Draw.sh yourname $PATH_STARPU $PATH_R_FOLDER
```
With $PATH_STARPU the path to the starpu folder
$PATH_R_FOLDER the path to the folder locality aware scheduling.
For example if you followed the step above you have: 
```bash
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/DEPOT/Draw.sh yourname $HOME/locality-aware-scheduling/ $HOME/locality-aware-scheduling/
```

If you used the Quick experiments you must run:
```bash
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/DEPOT/Quick_Draw.sh yourname $PATH_STARPU $PATH_R_FOLDER
```

## Running experiments on Simgrid

One of the 2D matrix multiplication, as well as the 3D matrix multiplication is done in simulation with Simgrid. A CHANGER SI BESOIN.

If you followed the step above you can do to generate it: 
```bash
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/DEPOT/Simulation.sh $HOME/locality-aware-scheduling/ $HOME/locality-aware-scheduling/ $HOME/locality-aware-scheduling/
```
Else it is:
```bash
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/DEPOT/Simulation.sh $PATH_SIMGRID $PATH_STARPU $PATH_R_FOLDER
```

All the graph created will be in the folder: /locality-aware-scheduling/R/Courbes/
